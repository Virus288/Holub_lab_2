package com.jakubKiszczyc.Service;

import com.jakubKiszczyc.Data.Models.Chat;
import com.jakubKiszczyc.Data.Payloads.Request.ChatRequest;
import com.jakubKiszczyc.Data.Payloads.Response.ChatResponse;
import com.jakubKiszczyc.Data.Repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChatServiceImpl implements ChatService {
    @Autowired
    ChatRepository chatRepository;

    @Override
    public ChatResponse createMessage(ChatRequest chatRequest) {
        Chat chat = new Chat();
        chat.setUser(chatRequest.getUser());
        chat.setMessage(chatRequest.getMessage());
        chatRepository.save(chat);
        return new ChatResponse("Message created");
    }

    @Override
    public ChatResponse updateMessage(Integer messageId, ChatRequest chatRequest) throws Exception {
        Optional<Chat> chat = chatRepository.findById(messageId);
        if (chat.isEmpty()){
            throw new Exception("Message not found");
        } else {
            chat.get().setUser(chatRequest.getUser());
            chat.get().setMessage(chatRequest.getMessage());
            chatRepository.save(chat.get());
            return new ChatResponse("Message updated");
        }
    }

    @Override
    public ChatResponse deleteMessage(Integer messageId) throws Exception {
        if (chatRepository.findById(messageId).get().getId() == messageId){
            chatRepository.deleteById(messageId);
            return new ChatResponse("Message was removed");
        } else {
            throw new Exception("Message not found");
        }
    }

    @Override
    public Chat getSingleMessage(Integer messageId) throws Exception {
        return chatRepository.findById(messageId).orElseThrow(() -> new Exception("Message not found"));
    }

    @Override
    public List<Chat> getAllMessages() {
        return chatRepository.findAll();
    }
}
