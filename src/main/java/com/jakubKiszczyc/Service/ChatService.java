package com.jakubKiszczyc.Service;

import com.jakubKiszczyc.Data.Models.Chat;
import com.jakubKiszczyc.Data.Payloads.Request.ChatRequest;
import com.jakubKiszczyc.Data.Payloads.Response.ChatResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface ChatService {

    ChatResponse createMessage(ChatRequest chatRequest);
    ChatResponse updateMessage(Integer messageId, ChatRequest chatRequest) throws Exception;
    ChatResponse deleteMessage(Integer messageId) throws Exception;
    Chat getSingleMessage(Integer messageId) throws Exception;
    List<Chat> getAllMessages();

}
