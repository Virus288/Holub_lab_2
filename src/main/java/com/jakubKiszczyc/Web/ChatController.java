package com.jakubKiszczyc.Web;

import com.jakubKiszczyc.Data.Models.Chat;
import com.jakubKiszczyc.Data.Payloads.Request.ChatRequest;
import com.jakubKiszczyc.Data.Payloads.Response.ChatResponse;
import com.jakubKiszczyc.Service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ChatController {
    @Autowired
    ChatService chatService;

    @GetMapping("/all")
    public List<Chat> getAllMessages(){
        return chatService.getAllMessages();
    }

    @GetMapping("/find/{id}")
    public Chat getMessageById(@PathVariable("id") Integer id) throws Exception {
        return chatService.getSingleMessage(id);
    }

    @PostMapping("/add")
    public ResponseEntity<ChatResponse> addNewMessage(@RequestBody ChatRequest message){
        ChatResponse newMessage = chatService.createMessage(message);
        return new ResponseEntity<>(newMessage, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ChatResponse updateMessageById(@PathVariable("id") Integer id, @RequestBody ChatRequest message) throws Exception {
        return chatService.updateMessage(id, message);
    }

    @DeleteMapping("/delete/{id}")
    public ChatResponse removeMessageById(@PathVariable("id") Integer id) throws Exception {
        return chatService.deleteMessage(id);
    }
}
