package com.jakubKiszczyc.Data.Repository;

import com.jakubKiszczyc.Data.Models.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Integer> {}