package com.jakubKiszczyc.Data.Models;

import jakarta.persistence.*;

@Entity
public class Chat {

    public Chat(){}

    public Chat(String user, String message){
        this.user = user;
        this.message = message;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String user;
    private String message;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString(){
        return "chat: id=" + id + ", user=" + user + ", message=" + message;
    }

}
