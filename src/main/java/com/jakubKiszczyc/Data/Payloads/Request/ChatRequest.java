package com.jakubKiszczyc.Data.Payloads.Request;

import com.jakubKiszczyc.Data.Models.Chat;
import com.sun.istack.NotNull;

public class ChatRequest {

    public ChatRequest(){}

    public ChatRequest(Chat chat){
        this.message = chat.getMessage();
        this.user = chat.getUser();
    }

    @NotNull
    private String user;
    @NotNull
    private String message;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
