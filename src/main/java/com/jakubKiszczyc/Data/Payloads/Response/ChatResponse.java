package com.jakubKiszczyc.Data.Payloads.Response;

public class ChatResponse {

    public ChatResponse(String message){
        this.message = message;
    }
    public ChatResponse(){

    }

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
