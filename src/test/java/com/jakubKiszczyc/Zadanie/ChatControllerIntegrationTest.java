package com.jakubKiszczyc.Zadanie;

import com.jakubKiszczyc.Data.Models.Chat;
import com.jakubKiszczyc.Data.Payloads.Request.ChatRequest;
import com.jakubKiszczyc.Data.Payloads.Response.ChatResponse;
import com.jakubKiszczyc.Data.Repository.ChatRepository;
import com.jakubKiszczyc.Web.ChatController;
import com.jakubKiszczyc.ZadanieApplication;
import org.assertj.core.internal.Classes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.SpringBootWebTestClientBuilderCustomizer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.data.domain.Example;

import java.util.List;
import java.util.Objects;

@SpringBootTest(classes = ZadanieApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ChatControllerIntegrationTest {

	@LocalServerPort
	private int port;

	@Autowired
	private ChatController controller;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private ChatRepository chatRepository;

	@AfterEach
	public void afterTest(){
		chatRepository.deleteAll();
	}

	@Test
	void contextLoads() {
		assert(controller != null);
	}

	@Test
	void addNewMessage() {
		ChatRequest message = new ChatRequest();
		message.setUser("Test");
		message.setMessage("Test message");
		ChatResponse response = restTemplate.postForObject("http://localhost:" + port + "/add", message, ChatResponse.class);
		assert(response.getMessage().equals("Message created"));
	}

	@Test
	void getAllMessage() {
		addMessage("Test", "Test message");
		addMessage("Test2", "Test message2");
		addMessage("Test3", "Test message3");

		assert(restTemplate.getForObject("http://localhost:" + port + "/all", List.class).size() == 3);
	}

	@Test
	void updateMessage() {
		Chat message = addMessage("Test", "Test message");
		Chat expectedMessage = chatRepository.findOne(Example.of(message)).get();

		ChatRequest messageRequest = new ChatRequest(message);
		messageRequest.setUser("Test2");

		restTemplate.put("http://localhost:" + port + "/update/" + expectedMessage.getId(), messageRequest);

		Chat actualMessage = chatRepository.findById(expectedMessage.getId()).get();

		assert(Objects.equals(expectedMessage.getId(), actualMessage.getId()));
		assert(actualMessage.getUser().equals("Test2"));
		assert(actualMessage.getMessage().equals(expectedMessage.getMessage()));
	}

	@Test
	void deleteMessage() {
		Chat message = addMessage("Test", "Test message");
		Chat expectedMessage = chatRepository.findOne(Example.of(message)).get();

		assert(chatRepository.findAll().size() == 1);

		restTemplate.delete("http://localhost:" + port + "/delete/" + expectedMessage.getId());

		assert(chatRepository.findAll().size() == 0);
	}

	private Chat addMessage(String user, String message){
		Chat newMessage = new Chat(user, message);
		ChatRequest newMessageRequest = new ChatRequest(newMessage);
		restTemplate.postForObject("http://localhost:" + port + "/add", newMessage, ChatResponse.class);
		return newMessage;
	}
}
