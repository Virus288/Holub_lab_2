# Create database
```
create table chat
(
id      int auto_increment,
user    varchar(50) not null,
message blob        not null,
constraint chat_pk
primary key (id)
);
```